from __future__ import print_function

import sys
from FrontRearDataset import FrontRearDataset
import torch
import torch.nn.functional as F
import torch.optim as optim
import matplotlib.pyplot as plt
import os
from datetime import datetime
import numpy as np
from importlib import import_module
from glob import glob


# -------------  Initialization  ------------- #
class Model:
    def __init__(self, model_dir):
        self.model_dir = model_dir
        sys.path.append(model_dir)
        net = import_module('net')
        self.args = net.args()
        use_cuda = not self.args.no_cuda and torch.cuda.is_available()
        torch.manual_seed(self.args.seed)
        self.device = torch.device("cuda" if use_cuda else "cpu")
        self.kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
        self.model = net.Net().to(self.device)

        list_epochs = glob('*.pth')
        if list_epochs == []:
            start_epoch = 1
        else:
            start_epoch = max([int(e.split('.')[0]) for e in list_epochs])
            self.model.load_state_dict(torch.load(os.path.join(model_dir, '{}.pth'.format(str(start_epoch)))))
            start_epoch += 1
        self.epoch = start_epoch

        data_transform = net.transform()

        self.train_loader = torch.utils.data.DataLoader(FrontRearDataset(
            csv_file='../../../code/names.csv', root_dir='../../../dataset',
            transform=data_transform), batch_size=self.args.batch_size, shuffle=True, **self.kwargs)
        self.test_loader = torch.utils.data.DataLoader(FrontRearDataset(
            csv_file='../../../code/names.csv', root_dir='../../../dataset', transform=data_transform),
            batch_size=self.args.batch_size, shuffle=True, **self.kwargs)
        self.optimizer = optim.SGD(self.model.parameters(), lr=self.args.lr, momentum=self.args.momentum)
        self.loss_vector = []
        self.plt_loss_vector = []

    def train(self):
        self.model.train()
        plt.clf()
        for batch_idx, (data, target) in enumerate(self.train_loader):
            data, target = data.to(self.device), target.to(self.device)
            self.optimizer.zero_grad()
            output = self.model(data)
            loss = F.mse_loss(output, target)
            loss.backward()
            self.optimizer.step()
            self.loss_vector.append(loss.item())
            if batch_idx % self.args.log_interval == 0:
                self.plt_loss_vector.append(np.mean(self.loss_vector))
                self.loss_vector[:] = []
                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                    self.epoch, batch_idx * len(data), len(self.train_loader.dataset),
                    100. * batch_idx / len(self.train_loader), self.plt_loss_vector[-1]))
                plt.plot(np.arange(0, len(self.plt_loss_vector)*self.args.log_interval, self.args.log_interval), self.plt_loss_vector,'ko-')
                plt.title('Epoch: %i' %self.epoch)
                plt.ylabel('MSE loss')
                plt.xlabel('Batch no. (batch size is %i)' %self.args.batch_size)
                plt.draw()
                plt.pause(0.01)

    def test(self):
        self.model.eval()
        test_loss = 0
        correct = 0
        with torch.no_grad():
            for batch_idx, (data, target) in enumerate(self.test_loader):
                data, target = data.to(self.device), target.to(self.device)
                output = self.model(data)
                # test_loss += F.mse_loss(output, target)
                # correct += output.eq(target.data).sum().item()

                # test_loss /= len(test_loader.dataset)
                # print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
                #    test_loss, test_loss, len(test_loader.dataset),
                #    100. * test_loss / len(test_loader.dataset)))
                # print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
                #    test_loss, correct, len(test_loader.dataset),
                #    100. * correct / len(test_loader.dataset)))

    def save(self):
        full_path = os.path.join(self.model_dir, '{}.pth'.format(str(self.epoch)))
        torch.save(self.model.state_dict(), full_path)


def main():
    model = Model(os.getcwd())

    for epoch in range(model.epoch, model.args.epochs + 1):
        model.epoch = epoch
        model.train()
        model.save()
        # test()

if __name__ == '__main__':
    main()


