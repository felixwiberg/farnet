from __future__ import print_function, division
import os
import pandas as pd
from skimage import io
from torch.utils.data import Dataset
import numpy as np

class FrontRearDataset(Dataset):
    """Front Rear dataset to use with FARNET"""

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with image-image pairs.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.image_pairs = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.image_pairs)

    def __getitem__(self, idx):
        img_name_front = os.path.join(self.root_dir, self.image_pairs.iloc[idx, 0])
        img_name_rear = os.path.join(self.root_dir, self.image_pairs.iloc[idx, 1])
        image_front = io.imread(img_name_front) #)color.rgb2lab(
        image_rear = (io.imread(img_name_rear)) #)color.rgb2lab(
        if self.transform:
            image_front = self.transform(image_front)
            image_rear = self.transform(image_rear)

        return image_front, image_rear
