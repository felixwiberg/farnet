from __future__ import print_function
from torch.autograd import Variable
import sys
from FrontRearDataset import FrontRearDataset
import torch
import torch.nn.functional as F
import torch.optim as optim
import matplotlib.pyplot as plt
import os
from datetime import datetime
import numpy as np
from importlib import import_module
from glob import glob
from skimage.color import rgb2luv, luv2rgb
import cv2
from torchvision import transforms
from Transforms import ToLab, ToPadded, ToTensor

# -------------  Initialization  ------------- #
class Model:
    def __init__(self, model_dir):
        self.model_dir = model_dir
        sys.path.append(model_dir)
        g_net = import_module('g_net')
        self.g_args = g_net.args()
        use_cuda = not self.g_args.no_cuda and torch.cuda.is_available()
        torch.manual_seed(self.g_args.seed)
        self.device = torch.device("cuda" if use_cuda else "cpu")
        self.kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
        self.g_model = g_net.Net().to(self.device)

        list_epochs = glob('*.pth')
        if list_epochs == []:
            start_epoch = 1
        else:
            print(list_epochs)
            start_epoch = max([int(e.split('_')[0]) for e in list_epochs])
            self.g_model.load_state_dict(torch.load(os.path.join(model_dir, '{}_g.pth'.format(str(start_epoch)))))

        data_transform = transforms.Compose([ToLab(), ToTensor()])

        self.test_loader = torch.utils.data.DataLoader(FrontRearDataset(
            csv_file='../../../code/names_val.csv', root_dir='../../../dataset_coil',
            transform=data_transform), batch_size=1, shuffle=False, **self.kwargs)
#self.g_args.batch_size
    def test(self):
        def l2r(im):
            im = im.double().detach().cpu().numpy()[0,:,:,:].T
            im = luv2rgb((im)*255-128)
            return np.rot90(im, k=1, axes=(1,0))

        fig = plt.figure()
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        #ax3 = fig.add_subplot(313)
        self.g_model.eval()
        plt.ioff()
        i=0

        for data, target in self.test_loader:
                data = data.to(self.device)
                output_g = self.g_model(data).detach()
                imout = l2r(output_g)
                imin = l2r(data)
                imtar = l2r(target)
                ax1.imshow(imout)
                ax2.imshow(imin)
                #ax3.imshow(imtar)
                #ax3.set_title('tar')

                filename = "t_%03i.png" % i
                ax1.axis('off')
                ax2.axis('off')
                ax1.set_title('Output')
                ax2.set_title('Input')
                plt.pause(0.001)
                fig.savefig(filename)
                plt.pause(0.001)
                plt.draw()
                ax1.cla()
                ax2.cla()
                i += 1


def main():
    model = Model(os.getcwd())
    model.test()


if __name__ == '__main__':
    main()


