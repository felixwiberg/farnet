from __future__ import print_function
from torch.autograd import Variable
import sys
from FrontRearDataset import FrontRearDataset
import torch
import torch.nn.functional as F
import torch.optim as optim
import matplotlib.pyplot as plt
import os
from datetime import datetime
import numpy as np
from importlib import import_module
from glob import glob
from skimage.color import rgb2luv, luv2rgb
import cv2

# -------------  Initialization  ------------- #
class Model:
    def __init__(self, model_dir):
        self.model_dir = model_dir
        sys.path.append(model_dir)
        g_net = import_module('g_net')
        d_net = import_module('d_net')
        self.g_args = g_net.args()
        self.d_args = d_net.args()
        use_cuda = not self.g_args.no_cuda and torch.cuda.is_available()
        torch.manual_seed(self.g_args.seed)
        self.device = torch.device("cuda" if use_cuda else "cpu")
        self.kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
        self.g_model = g_net.Net().to(self.device)
        self.d_model = d_net.Net().to(self.device)

        list_epochs = glob('*.pth')
        if list_epochs == []:
            start_epoch = 1
        else:
            print(list_epochs)
            start_epoch = max([int(e.split('_')[0]) for e in list_epochs])
            self.g_model.load_state_dict(torch.load(os.path.join(model_dir, '{}_g.pth'.format(str(start_epoch)))))
            self.d_model.load_state_dict(torch.load(os.path.join(model_dir, '{}_d.pth'.format(str(start_epoch)))))
            start_epoch += 1
        self.epoch = start_epoch

        data_transform = g_net.transform()

        self.train_loader = torch.utils.data.DataLoader(FrontRearDataset(
            csv_file='../../../code/names_train.csv', root_dir='../../../dataset',
            transform=data_transform), batch_size=self.g_args.batch_size, shuffle=True, **self.kwargs)

        self.val_loader = torch.utils.data.DataLoader(FrontRearDataset(
            csv_file='../../../code/names_val.csv', root_dir='../../../dataset',
            transform=data_transform), batch_size=self.g_args.batch_size, shuffle=True, **self.kwargs)

        self.g_optimizer = optim.SGD(self.g_model.parameters(), lr=self.g_args.lr, momentum=self.g_args.momentum)
        self.d_optimizer =  optim.SGD(self.d_model.parameters(), lr=self.d_args.lr, momentum=self.g_args.momentum)

        self.plt_loss_vector_g = []
        self.plt_loss_vector_d = []
        self.plt_loss_vector_v = []
        self.plotrange_t = []
        self.plotrange_v = []

        self.fig1 = plt.figure(1)
        self.fig2 = plt.figure(2)
        self.ax1_g = self.fig1.add_subplot(111)
        self.ax1_d = self.ax1_g.twinx()
        self.ax2 = self.fig2.add_subplot(311)
        self.ax3 = self.fig2.add_subplot(312)
        self.ax4 = self.fig2.add_subplot(313)
        self.ax1_g.set_ylabel('MSE loss')
        self.ax1_g.set_xlabel('Batch no. (batch size is %i)' %self.g_args.batch_size)
        self.ax1_g.plot([],[], 'ko-',label='Generator Training Loss')
        self.ax1_g.plot([],[], 'bo--',label='Descriminator Training Loss')
        self.ax1_g.plot([],[], 'r*-',label='Generator Validation Loss')
        self.ax1_g.legend()

    def train(self):
        def l2r(im):
            im = im.double().detach().cpu().numpy()[0,:,:,:].T
            im = luv2rgb((im)*255-128)
            return np.rot90(im, k=1, axes=(1,0))


        output_d = Variable(torch.ones(1))
        self.g_model.train()
        self.d_model.train()
        loss_vector_g = []
        loss_vector_d = []

        for batch_idx, (data, target) in enumerate(self.train_loader):
            s = 0.001

            if (batch_idx%2==0) and (torch.mean(output_d) <= 0.55): #training discriminator on generators output
                print('d trained!', s*torch.mean(output_d))
                data, target = data.to(self.device), target.to(self.device)
                self.d_optimizer.zero_grad()

                output_g = self.g_model(data).detach()
                output_d_generated = self.d_model(output_g)
                d_loss_generated = F.mse_loss(output_d_generated, Variable(torch.ones(self.g_args.batch_size).to(self.device)))
                d_loss_generated.backward()

                output_d_real = self.d_model(target)
                d_loss_real = F.mse_loss(output_d_real, Variable(torch.zeros(self.g_args.batch_size).to(self.device)))
                d_loss_real.backward()

                self.d_optimizer.step()
            else:
                data, target = data.to(self.device), target.to(self.device)
                self.g_optimizer.zero_grad()
                output_g = self.g_model(data)
                output_d = self.d_model(output_g).detach()
                g_loss = F.mse_loss(output_g, target)
                g_loss_fixed = torch.add(g_loss, s*torch.mean(output_d))
                g_loss_fixed.backward()
                self.g_optimizer.step()
                loss_vector_g.append(g_loss.item())
                loss_vector_d.append(s*torch.mean(output_d).item())

            if batch_idx % self.g_args.log_interval  == 0 and batch_idx > 0:
                self.plt_loss_vector_g.append(np.mean(loss_vector_g))
                self.plt_loss_vector_d.append(np.mean(loss_vector_d))
                loss_vector_g = []
                loss_vector_d = []

                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                    self.epoch, batch_idx * len(data), len(self.train_loader.dataset),
                    100. * batch_idx / len(self.train_loader), self.plt_loss_vector_g[-1]))
                self.plotrange_t = np.arange(0, len(self.plt_loss_vector_g)*self.g_args.log_interval, self.g_args.log_interval)
                self.ax1_g.semilogy(self.plotrange_t, self.plt_loss_vector_g,'ko-')
                self.ax1_d.semilogy(self.plotrange_t, self.plt_loss_vector_d, 'bo--')
                self.ax1_g.set_title('Epoch: %i' %self.epoch)

                imout = l2r(output_g)
                imin = l2r(data)
                imtar = l2r(target)

                self.ax2.imshow(imout)
                self.ax2.set_title('Network Output')
                self.ax3.imshow(imin)
                self.ax3.set_title('Network Input')
                self.ax4.imshow(imtar)
                self.ax4.set_title('Target')


                plt.draw()
                plt.pause(0.001)
                self.ax2.clear()
                self.ax3.clear()
                self.ax4.clear()


    def validate(self):
        self.g_model.eval()
        self.d_model.eval()
        test_loss = 0
        correct = 0
        loss_vector_v = []
        with torch.no_grad():
            s = 0.001
            for batch_idx, (data, target) in enumerate(self.val_loader):

                data, target = data.to(self.device), target.to(self.device)
                self.g_optimizer.zero_grad()
                output_g = self.g_model(data)
                output_d = self.d_model(output_g).detach()
                g_loss = F.mse_loss(output_g, target)
                g_loss_fixed = torch.add(g_loss, s*torch.mean(output_d))

                loss_vector_v.append(g_loss.item())

        self.plt_loss_vector_v.append(np.mean(loss_vector_v))
        self.plotrange_v.append(np.max(self.plotrange_t))
        print(self.plotrange_v)
        self.ax1_g.semilogy(self.plotrange_v, self.plt_loss_vector_v,'r*-')


        self.fig1.tight_layout()
        self.fig2.tight_layout()

        plt.draw()
        self.fig1.savefig("1a.png")
        self.fig2.savefig("2b.png")
        plt.pause(0.001)


    def save(self):
        full_path_d = os.path.join(self.model_dir, '{}_d.pth'.format(str(self.epoch)))
        full_path_g = os.path.join(self.model_dir, '{}_g.pth'.format(str(self.epoch)))
        torch.save(self.g_model.state_dict(), full_path_g)
        torch.save(self.d_model.state_dict(), full_path_d)


def main():
    model = Model(os.getcwd())

    for epoch in range(model.epoch, model.g_args.epochs + 1):
        #if epoch%10 == 0:
        #    model.save()
        model.epoch = epoch
        model.train()
        model.validate()


if __name__ == '__main__':
    main()


