import glob
import torch
import scipy  as sp
import matplotlib.image as mpimg
import numpy as np
import csv

def get_between(s, first, last):
    try:
        start = s.index( first ) + len( first )
        end = s.index(last, start)
        return int(s[start:end])
    except ValueError:
        return ""

pathlist = glob.glob('../dataset/*')
pathlist_train = []
pathlist_val = []
buddylist_train = []
buddylist_val = []



for ipath, path in enumerate(pathlist):

    angle = get_between(path, '_r', '.')
    if(angle > 179):
        buddy_angle = angle - 180
    else:
        buddy_angle = angle + 180
    imno = get_between(path, 't/', '_')
    if ipath < np.round(len(pathlist)*0.9):
        buddylist_train.append('../dataset/'+str(imno)+'_r'+str(buddy_angle)+'.png')
        pathlist_train.append(path)
    else:
        buddylist_val.append('../dataset/'+str(imno)+'_r'+str(buddy_angle)+'.png')
        pathlist_val.append(path)


with open('names_train.csv', 'w') as csvfile:
    fieldnames = ['file1_name', 'file2_name']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for i in range(len(pathlist_train)):
            writer.writerow({'file1_name': pathlist_train[i], 'file2_name': buddylist_train[i]})


with open('names_val.csv', 'w') as csvfile:
    fieldnames = ['file1_name', 'file2_name']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for i in range(len(pathlist_val)):
        writer.writerow({'file1_name': pathlist_val[i], 'file2_name': buddylist_val[i]})
