import glob
import torch
import scipy  as sp
import matplotlib.image as mpimg
import numpy as np
import csv
from skimage import io
from skimage.color import rgb2luv, luv2rgb
import cv2

def get_between(s, first, last):
    try:
        start = s.index( first ) + len( first )
        end = s.index(last, start)
        return int(s[start:end])
    except ValueError:
        return ""

pathlist = glob.glob('../dataset_coil/*')
mean = [0,0,0]
std = [0,0,0]
l = 1000 #len(pathlist)
print('Calculating Mean and STD of dataset, stand by')

for path in pathlist[0:1000]:
    img = (rgb2luv(io.imread(path))+128)/255
    #print(np.mean(img[:,:,2]))
    mean = np.add(mean, np.divide([np.mean(img[:,:,0]), np.mean(img[:,:,1]), np.mean(img[:,:,2])],l))
    std = np.add(std, np.divide([np.std(img[:,:,0]), np.std(img[:,:,1]), np.std(img[:,:,2])],l))

print('mean: ', mean)
print('std: ', std)
