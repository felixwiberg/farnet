import argparse
import torch.nn as nn
import torch.nn.functional as F
from torchvision import transforms

# -------------  Network class  ------------- #
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 7, padding=3)
        self.conv2 = nn.Conv2d(6, 12, 7, padding=3)
        self.conv3 = nn.Conv2d(12, 24, 7, padding=3)
        self.conv4 = nn.Conv2d(24, 48, 7, padding=3)
        self.conv5 = nn.Conv2d(48, 24, 7, padding=3)
        self.conv6 = nn.Conv2d(24, 12, 7, padding=3)
        self.conv7 = nn.Conv2d(12, 6, 7, padding=3)
        self.conv8 = nn.Conv2d(6, 3, 7, padding=3)
        self.bn6a = nn.BatchNorm2d(6, affine=False)
        self.bn12a = nn.BatchNorm2d(12, affine=False)
        self.bn24a = nn.BatchNorm2d(24, affine=False)
        self.bn48a = nn.BatchNorm2d(48, affine=False)
        self.bn3b = nn.BatchNorm2d(3, affine=False)
        self.bn6b = nn.BatchNorm2d(6, affine=False)
        self.bn12b = nn.BatchNorm2d(12, affine=False)
        self.bn24b = nn.BatchNorm2d(24, affine=False)
        self.pool1 = nn.MaxPool2d(2, 2, return_indices=True)
        self.pool2 = nn.MaxPool2d(2, 2, return_indices=True)
        self.unpool1 = nn.MaxUnpool2d(2, 2)
        self.unpool2 = nn.MaxUnpool2d(2, 2)

    def forward(self, x):
        x = F.relu(self.bn6a(self.conv1(x)))
        x = F.relu(self.bn12a(self.conv2(x)))
        x = F.relu(self.bn24a(self.conv3(x)))
        x = F.relu(self.bn48a(self.conv4(x)))

        size2 = x.size()
        x, ind2 = self.pool2(x)
        x = self.unpool1(x, ind2, output_size=size2)

        x = F.relu(self.bn24b(self.conv5(x)))
        x = F.relu(self.bn12b(self.conv6(x)))
        x = F.relu(self.bn6b(self.conv7(x)))
        x = F.relu(self.bn3b(self.conv8(x)))
        return x


# -------------  Training settings  ------------- #
def args():
    parser = argparse.ArgumentParser(description='PyTorch FARNET')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
            help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
            help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=50, metavar='N',
            help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.005, metavar='LR',
            help='learning rate (default: 0.01)')
    parser.add_argument('--momentum', type=float, default=0.3, metavar='M',
            help='SGD momentum (default: 0.5)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
            help='disables CUDA training')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
            help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=20, metavar='N',
            help='how many batches to wait before logging training status')
    args = parser.parse_args()
    return args

def transform():
    data_transform = transforms.Compose([
        transforms.ToTensor(),
        #    transforms.Normalize(mean=[27.79037, 23.41402, 15.53410], std=[41.36963, 33.87234, 25.92335])
        ])
    return data_transform
