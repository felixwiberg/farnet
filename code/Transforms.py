from skimage import io
from skimage.color import rgb2luv, luv2rgb
import numpy as np
import torch
import cv2
from matplotlib import pyplot as plt

class ToLab(object):
    """Convert samples to LAB."""

    def __call__(self, rgb):
        luv = (rgb2luv(rgb)+128)/255
        return luv


class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        #image, landmarks = sample['image'], sample['landmarks']
        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        sample = sample.transpose((2, 0, 1))
        return torch.from_numpy(sample).float()


class ToPadded(object):
    """Normalize image in LUV-space."""

    def __call__(self, sample):
        sample -= [0.04, 0, 0]
        #sample /= [1.1, 1.1, 1.1]
        print('sample ', np.shape(sample))
        padded = np.pad(sample, ((8, 8), (32, 32), (0,0)), 'constant', constant_values=(0.5))
        print('padded ', np.shape(padded))
        return padded
