
from __future__ import print_function
from FrontRearDataset import FrontRearDataset
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import transforms
import matplotlib.pyplot as plt
import os
from datetime import datetime
import numpy as np
# -------------  Network class  ------------- #
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 7, padding=3)
        self.conv2 = nn.Conv2d(6, 12, 7, padding=3)
        self.conv3 = nn.Conv2d(12, 24, 7, padding=3)
        self.conv4 = nn.Conv2d(24, 12, 7, padding=3)
        self.conv5 = nn.Conv2d(12, 6, 7, padding=3)
        self.conv6 = nn.Conv2d(6, 3, 7, padding=3)
        self.bn6 = nn.BatchNorm2d(6, affine=False)
        self.bn12 = nn.BatchNorm2d(12, affine=False)
        self.bn24 = nn.BatchNorm2d(24, affine=False)
        self.bn62 = nn.BatchNorm2d(6, affine=False)
        self.bn122 = nn.BatchNorm2d(12, affine=False)
        self.bn242 = nn.BatchNorm2d(24, affine=False)
        self.pool1 = nn.MaxPool2d(2, 2, return_indices=True)
        self.pool2 = nn.MaxPool2d(2, 2, return_indices=True)
        self.unpool1 = nn.MaxUnpool2d(2, 2)
        self.unpool2 = nn.MaxUnpool2d(2, 2)

    def forward(self, x):
        x = F.relu(self.bn6(self.conv1(x)))
        x = F.relu(self.bn12(self.conv2(x)))

        size1 = x.size()
        x, ind1 = self.pool1(x)

        x = F.relu(self.bn24(self.conv3(x)))

        size2 = x.size()
        x, ind2 = self.pool2(x)

        x = self.unpool1(x, ind2, output_size=size2)
        x = F.relu(self.bn122(self.conv4(x)))

        x = self.unpool2(x, ind1, output_size=size1)
        x = F.relu(self.bn62(self.conv5(x)))
        x = F.relu((self.conv6(x)))

        return x


def initialize_model(model_name):
    if '/' in model_name:
        date, model_name = model_name.split('/')
    else:
        date = datetime.now().strftime('%m%d')
    root_dir = os.path.join(os.getcwd(), '..')
    models_dir = os.path.join(root_dir, 'models')
    date_dir = os.path.join(models_dir, date)
    model_dir = os.path.join(date_dir, model_name)

    model = Net()

    epoch = max([int(e) for e in os.listdir(model_dir)])
    model.load_state_dict(torch.load(os.path.join(model_dir, str(epoch))))

    data_transform = transforms.Compose([transforms.ToTensor()])

    test_loader = torch.utils.data.DataLoader(
            FrontRearDataset(csv_file='names.csv', root_dir=os.path.join(root_dir, 'dataset'), transform=data_transform),
            shuffle=True)

    return model, test_loader


def evaluate(model, test_loader):
    model.eval()
    for data, target in test_loader:
        output = model(data).detach()
        plt.subplot(121)
        plt.imshow(get_image(target))
        plt.axis('off')
        plt.subplot(122)
        plt.imshow(get_image(output))
        plt.axis('off')
        plt.pause(1)


def get_image(image_tensor):
    return np.transpose(image_tensor[0, :, :, :].numpy(), (1, 2, 0))


def main():
    model_name = 'test'
    model, test_loader = initialize_model(model_name)
    evaluate(model, test_loader)


if __name__ == '__main__':
    main()
