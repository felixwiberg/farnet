#!/bin/bash

a=`date +%m%d`
mkdir ../models/$a
mkdir ../models/$a/$1
cp g_net_master.py ../models/$a/$1/g_net.py
cp d_net_master.py ../models/$a/$1/d_net.py
cp run_GAN.sh ../models/$a/$1/run_GAN.sh

cd ../models/$a/$1

