import glob
import torch
import scipy  as sp
import matplotlib.image as mpimg
import numpy as np
import csv

def get_between(s, first, last):
    try:
        start = s.index( first ) + len( first )
        end = s.index(last, start)
        return int(s[start:end])
    except ValueError:
        return ""

pathlist = glob.glob('../dataset/*')
pathlist_train = []
pathlist_val = []
buddylist_train = []
buddylist_val = []
pathlist = pathlist[0:73]

angle = 0

for ipath, path in enumerate(pathlist):
    if(angle > 179):
        buddy_angle = angle - 180
    else:
        buddy_angle = angle + 180
    imno = get_between(path, 't/', '_')
    pathlist_val.append('../dataset/'+'55'+'_r'+str(angle)+'.png')
    buddylist_val.append('../dataset/'+'55'+'_r'+str(buddy_angle)+'.png')
    angle += 5

with open('names_val_rotate.csv', 'w') as csvfile:
    fieldnames = ['file1_name', 'file2_name']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for i in range(len(pathlist_val)):
        writer.writerow({'file1_name': pathlist_val[i], 'file2_name': buddylist_val[i]})
