import argparse
import torch.nn as nn
import torch.nn.functional as F
from torchvision import transforms
from Transforms import ToLab

# -------------  Network class  ------------- #
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 7, padding=3)
        self.conv2 = nn.Conv2d(6, 1, 7, padding=3)
        self.bn6 = nn.BatchNorm2d(6, affine=False)
        self.bn3 = nn.BatchNorm2d(1, affine=False)
        self.pool = nn.MaxPool2d(2, 2, return_indices=True)
        self.unpool = nn.MaxUnpool2d(2, 2)
        self.map1 = nn.Linear(256*27, 64)
        self.map2 = nn.Linear(64, 1)

    def forward(self, x):
        print(x.size())
        x = F.relu(self.bn6(self.conv1(x)))
        print(x.size())
        x, ind = self.pool(x)
        print(x.size())
        x = F.relu(self.bn3(self.conv2(x)))
        print(x.size())
        x = x.view(10, 256*27)
        print(x.size())
        x = F.relu(self.map1(x))
        print(x.size())
        x = F.sigmoid(self.map2(x))
        print(x.size())
        x = x.view(10)
        return x


# -------------  Training settings  ------------- #
def args():
    parser = argparse.ArgumentParser(description='PyTorch FARNET')
    parser.add_argument('--batch-size', type=int, default=32, metavar='N',
            help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
            help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=50, metavar='N',
            help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.001, metavar='LR',
            help='learning rate (default: 0.01)')
    parser.add_argument('--momentum', type=float, default=0.0, metavar='M',
            help='SGD momentum (default: 0.5)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
            help='disables CUDA training')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
            help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=20, metavar='N',
            help='how many batches to wait before logging training status')
    args = parser.parse_args()
    return args

def transform():
    data_transform = transforms.Compose([
        ToLab(),
        ToTensor(),
        #    transforms.Normalize(mean=[27.79037, 23.41402, 15.53410], std=[41.36963, 33.87234, 25.92335])
        ])
    return data_transform
