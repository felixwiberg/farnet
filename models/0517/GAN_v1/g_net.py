import argparse
import torch.nn as nn
import torch.nn.functional as F
from torchvision import transforms
from Transforms import ToLab, ToTensor

# -------------  Network class  ------------- #
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 30, 9, padding=5)
        self.conv2 = nn.Conv2d(30, 60, 7, padding=3)
        self.conv3 = nn.Conv2d(60, 60, 7, padding=3)
        self.conv4 = nn.Conv2d(60, 60, 7, padding=3)
        self.conv5 = nn.Conv2d(60, 60, 7, padding=3)
        self.conv6 = nn.Conv2d(60, 60, 7, padding=3)
        self.ct1 = nn.ConvTranspose2d(60,60,4,stride=2,padding=1)
        self.ct2 = nn.ConvTranspose2d(60,60,4,stride=2,padding=1)
        self.ct3 = nn.ConvTranspose2d(60,60,4,stride=2,padding=1)
        self.ct4 = nn.ConvTranspose2d(60,60,4,stride=2,padding=1)
        self.conv7 = nn.Conv2d(60, 60, 3, padding=1)
        self.conv10 = nn.Conv2d(60, 60, 3, padding=1)
        self.conv8 = nn.Conv2d(60, 30, 3, padding=1)
        self.conv9 = nn.Conv2d(30, 3, 3, padding=1)
        self.bn1 = nn.BatchNorm2d(30, affine=False)
        self.bn2 = nn.BatchNorm2d(60, affine=False)
        self.bn3 = nn.BatchNorm2d(60, affine=False)
        self.bn4 = nn.BatchNorm2d(60, affine=False)
        self.bn5 = nn.BatchNorm2d(60, affine=False)
        self.bn6 = nn.BatchNorm2d(60, affine=False)
        self.bn7 = nn.BatchNorm2d(60, affine=False)
        self.bn8 = nn.BatchNorm2d(60, affine=False)
        self.bn9 = nn.BatchNorm2d(30, affine=False)
        self.bn10 = nn.BatchNorm2d(60, affine=False)
        #self.bn11 = nn.BatchNorm2d(60, affine=False)

        self.pool1 = nn.MaxPool2d(2, 2)
        self.pool2 = nn.MaxPool2d(2, 2)
        self.pool3 = nn.MaxPool2d(2, 2)
        self.pool4 = nn.MaxPool2d(2, 2)
        self.map1 = nn.Linear(6480, 6480)
        self.bnlinear = nn.BatchNorm1d(6480, affine=False)
        self.map2 = nn.Linear(6480, 6480)
        self.map3 = nn.Linear(6480, 6480)

    def forward(self, x):
        #print(x.size())
        x = self.bn1(F.relu(self.conv1(x)))
        #print(x.size())
        x = self.bn2(F.relu(self.conv2(x)))
        #print(x.size())
        x = self.pool1(x)
        #print(x.size())
        x = self.bn3(F.relu(self.conv3(x)))
        #print(x.size())
        x = self.pool2(x)
        #print(x.size())
        x = self.bn4(F.relu(self.conv4(x)))
        #print(x.size())

        x = self.pool3(x)
        #print(x.size())
        x = self.bn5(F.relu(self.conv5(x)))
        #print(x.size())

        x = self.pool4(x)
        #print(x.size())
        x = self.bn6(F.relu(self.conv6(x)))
        #print(x.size())
        p = x.size()
        (_, C, H, W) = x.data.size()
        x = x.view( -1 , C * H * W)
        x = F.relu(self.map1(x))
        #print(x.size())
        x = F.relu(self.map2(x))
        #print(x.size())
        x = F.relu(self.map3(x))
        #print(x.size())
        x = x.view(p)
        x =(F.relu((self.ct1(x))))
        #print(x.size())
        x =(F.relu((self.ct2(x))))
        #print(x.size())
        x = self.bn10(F.relu(self.conv10(x)))
        #print(x.size())
        x =(F.relu((self.ct3(x))))
        #print(x.size())
        x = self.bn7(F.relu(self.conv7(x)))
        #print(x.size())
        x = self.bn8(F.relu(self.ct4(x)))
        #print(x.size())
        x = (F.relu(self.bn9(self.conv8(x))))
        #print(x.size())
        x = F.relu((self.conv9(x)))
        #print(x.size())
        return x


# -------------  Training settings  ------------- #
def args():
    parser = argparse.ArgumentParser(description='PyTorch FARNET')
    parser.add_argument('--batch-size', type=int, default=10, metavar='N',
            help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
            help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=500, metavar='N',
            help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
            help='learning rate (default: 0.1)')
    parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
            help='SGD momentum (default: 0.5)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
            help='disables CUDA training')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
            help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=3000, metavar='N',
            help='how many batches to wait before logging training status')
    args = parser.parse_args()
    return args

def transform():
    data_transform = transforms.Compose([
        ToLab(),
        #ToNormalized(),
        ToTensor(),
        ])
    return data_transform
