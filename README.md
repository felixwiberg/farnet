FarNet is a network that guesses the far side of objects

    Style Guide

Methods and variables are named like this:
    'my_function()'
    'my_variable'

Classes are named likt this:
    'MyClass'

Filenames (modules) does not need to have the same name as the classes they obtain.
Filenames should be all lowercase and short, underscores are not prohibited but frowned upon.
